# Questions ICP

- script sec. 4.1.1: script says is second order phase transition and order parameter P(p) is discontinuous, but in fig. 4.3 the order parameter is continuous at the critical point.
  - A: got it the wrong way round. if continuous then it is a second-order transition (its first derivative will be discontinuous)
- Burning Algorithm: p. 41, how do we determine order parameter (percolation strength, number of sites belonging to infinite cluster / in finite case spanning cluster?) from Burning algorithm?
    - A: it requires additional work. traverse lattice from bottom up and count. (set bottom site to some value, then set its neighbours to same value. increase counter for every neighbour you find. repeat for neighbours. do until you run of neighbours)
- ch4.2.3: why does the finite-size scaling algorithm make sense? what does data collapse mean and how can we exploit it?
- why can we regard percolating clusters as fractals? why does the coherence length diverging at the critical point imply that we cannot determine the phase? why does it mean that we can 'zoom' out and the cluster will look the same?
- p.70, MC methods: how to derive errors for trapezoidal rule? how to derive errors for 1D MC and nD MC?
- p. 105: why do forward-backward timestepping for shallow water and not forward-forward or backward-backward?
- p. 105: eq. 8.113: should denominator on RHS not be deltaX (instead of 2*deltaX)? spatial step is from j+1/2 to j-1/2, so only of deltaX


## Slides

### Chapter 8
- 41/44: phase velocity: the fuck is going on here?
- ch8 cont: 41: don't understand how FFT saves us computation cylces

### Chapter 9
- do we do Boris for every particle or the macro-particles we're talking about in PIC?

### Chapter 10
- where does Vlasov equation actually show up? we only look at maxwell equations / Lorentz force afterwards...
- what is interpretation of first quantity on slide 7? (integral of squared dist. function?)
- slide 13: why use tangens? it didn't show up anywhere before and its probably harder to calculate than the direct expression
- slide 23: where did J vanish to? why is it not included in update scheme (or why does it vanish?)
- slide 31: why does wave not travel with c but with c/2?

### Chapter 11
- in Barnes-Hut algorithm: how large do we choose the squares where we calculate the c_m, t_m?
    - we calculate it for all squares / all nodes of the tree. we use different nodes to calculate the force depending on which particle we're working with
