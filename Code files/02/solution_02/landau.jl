using Roots
using Distributions

# the not-normalized cumulative distribution function of p(x) = 1 + cos(x/2) / 2
I(x) = x + sin(x/2) 


"The cumulative distribution function at value x for a slice between x1 and x2"
function I_slice(x; slice=(0,4pi))
	# error checks
	x1, x2 = slice
	x < x1 ? error("the input x is not in the defined slice!") : nothing
	x1 > x2 ? error("slice not defined correctly!") : nothing
	
	# normalization of the CDF in our slice
	norm =  I(x2) - I(x1)
	
	return (I(x) - I(x1) ) / norm
end


"Generate a random particle in a slice"
function randomLandau(slice=(0, 4pi))
    u = rand(Uniform(0,1))
    this_i(x) = I_slice(x, slice=slice) - u
    roots = find_zeros(this_i, slice...)
    length(roots) > 1 && error("Multiple roots found!") || return first(roots)
end


"Generate n random particles in a slice, possibly multi-threaded"
function randomLandau(n::Int64; slice=(0,4pi), threaded=false)
    if threaded
        # an uninitialized array
        S = Vector{Float64}(undef, n)
        Threads.@threads for i in 1:n
            S[i] = randomLandau(slice)
        end
    else
        S = [randomLandau(slice) for i in 1:n]
    end
    return S
end







