# Computational Physics
# Sheet 02, Exercise 1

# Task 1

using Roots
using Random
using Plots

### Function definitions ###

function I(y)
    return 1/4*pi * (y + sin(y/2.0))
end

function IInverse(y; r)
    return I(y) - r
end

function g(x)
    return 1 + 1/2.0 * cos(x/2.0)
end

##############

slice = [0, 4*pi]
x = zeros((5000))

rng = MersenneTwister(3333)
rand!(rng, x)

# generate the random numbers x'
xprime = zeros(size(x)[1])
for i in 1:size(x)[1]
    function Iprime(y)
        # why do we normalize I(y)??
        # we normalize st the generated random variables are between 0 and 1
        # we subtract smth between 0 and 1, so anything else wouldn't make sense?
        return I(y) / (slice[2] - slice[1]) - x[i]
    end
    xprime[i] = find_zero(Iprime, 2*pi, Order1())
end

# scale x' s.t. they 
histogram(xprime, label="x'")
histogram(x, label="x")

plot(0:0.001:1, @.g(0:0.001:1))


    