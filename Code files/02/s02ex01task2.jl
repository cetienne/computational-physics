# Computational Physics
# Set 02, Exercise 01, Task 2

using Random
using Plots
using Roots
using BenchmarkTools

### Function definitions ###

function I(y)
    return 1/4*pi * (y + sin(y/2.0))
end

function IInverse(y; r)
    return I(y) - r
end

function g(x)
    return 1 + 1/2.0 * cos(x/2.0)
end

slice = [0, 4*pi]

function sampling(x, xprime, no_particles)
    Threads.@threads for i in 1:no_particles
        id = Threads.threadid()
        println("Using thread $id")
        u = x[Int64(i)]
        Iprime(y) = I(y) / (slice[2] - slice[1]) - u
        xprime[Int64(i)] = find_zero(Iprime, 2*pi, Order1())

    end
end

function sampling_1thread(x, xprime, no_particles)
    for i in 1:no_particles
        u = x[Int64(i)]
        Iprime(y) = I(y) / (slice[2] - slice[1]) - u
        xprime[Int64(i)] = find_zero(Iprime, 2*pi, Order1())
    end
end



#= p(x) = (1 + cos(x/2) / 2) / 4pi

x_axis = LinRange(0, 4pi, 100)
p(x) = (1 + cos(x/2) / 2) / 4pi # the normalized p.d. we are after

histogram(xprime, normalize=true, label="sampling", bins=0:4pi)
plot!(x_axis, p.(x_axis), label="probability distribution", linewidth=7, xaxis="x", yaxis="frequency")
savefig("task1_histogram.pdf") =#

particles = range(1000, 10000; length=11)

S = zeros(size(particles)[1])

particles 

for (index, no_particles) in enumerate(particles)
    # define random variables
    xprime = zeros(Int64(floor(no_particles)))
    u = zeros((Int64(floor(no_particles))))
    rng = MersenneTwister(3333)
    rand!(rng, u)
    # run simulation for multi-threading
    t_mt = @benchmark sampling($u, $xprime, $no_particles)
    # run simulation for single-threading
    t_st = @benchmark sampling_1thread($u, $xprime, $no_particles)
    S[index] = mean(t_st.times) / mean(t_mt.times)
end

plot(particles, S, label="speedup", xlabel="no of particles", ylabel="speedup")
savefig("task2_speedup.pdf")
