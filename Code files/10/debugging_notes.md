# Debugging Notes

# Set 10, Exercise 01
# Shallow Water equation

## Open Questions
- whats the double derivative for velocity? sum over both directions or only one?


## Thoughts
- first time step seems to work fine (velocity heatmaps makes sense). wave seems to flow outwards. BUT: height doesn't really change at all, which is strange.
- also, at second time step everything goes to shit, arrays are filled with NANs. => Problem with boundary conditions??