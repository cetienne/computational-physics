# Compuatational Physics
# Set 10, Exercise 01
# Shallow Water equation

# Author(s): Etienne Corminboeuf

using Plots

global g = 9.81
global b = 0.2
global nu = 1e-3

function at(arr, Inds)
    # periodic boundary conditions
    # only works for quadratic arrays!!
    N = size(arr, 1)
    if checkbounds(Bool, arr, Inds...)
        return arr[Inds...]
    else
        Inds = collect(Inds)
        Inds = replace(Inds, 0 =>N)
        Inds = replace(Inds, N+1 =>1)
        Inds = Tuple(Inds)
        return arr[Inds...]
    end
end

function shallowWater2DStep(h, v, deltax, deltaT)

    (Nx, Ny) = size(h)
    hnew = zeros((Nx, Ny))
    vnew = zeros((Nx, Ny, 2))

    for i in 1:Nx

        for j in 1:Ny
            
            # wave height
            h_xt = at(h, (i,j))
            h_xmdelta = at(h, (i, j-1))
            h_xpdelta = at(h, (i, j+1))

            h_ymdelta = at(h, (i-1, j))
            h_ypdelta = at(h, (i+1, j))
            
            # wave velocity

            v_t = v[i, j,:]
            v_xmdelta = at(v, (i, j-1,:))
            v_xpdelta = at(v, (i, j+1,:))

            v_ymdelta = at(v, (i-1, j,:))
            v_ypdelta = at(v, (i+1, j,:))
            
            # PDE for h
            d1hv = (h_xpdelta * v_xpdelta[1] - h_xmdelta * v_xmdelta[1])/(2*deltax)
            d2hv = (h_ypdelta * v_ypdelta[2] - h_ymdelta * v_ymdelta[2])/(2*deltax)

            dth = - d1hv - d2hv  # PDE

            # PDE for v
            d1h = (h_xpdelta - h_xmdelta)/(2*deltax)
            d2h = (h_ypdelta - h_ymdelta)/(2*deltax)
            d1v = (v_xpdelta - v_xmdelta)/(2*deltax)
            d2v = (v_ypdelta - v_ymdelta)/(2*deltax)
            
            dd1v = (v_xpdelta - 2* v_t + v_xmdelta)/(deltax^2)
            dd2v = (v_ypdelta - 2* v_t + v_ymdelta)/(deltax^2)

            dtv1 = -g*d1h - v_t[1] * d1v[1] - v_t[2]*d2v[1] - b*v_t[1] + nu * dd1v[1] + nu * dd2v[2]  # PDE
            dtv2 = -g*d2h - v_t[1] * d1v[2] - v_t[2]*d2v[2] - b*v_t[2] + nu * dd2v[2] + nu * dd1v[1] # PDE

            # integrate the PDEs
            # explicit Euler

            htp1 = h_xt + deltaT * dth
            v1_tp1 = v_t[1] + deltaT * dtv1
            v2_tp1 = v_t[2] + deltaT * dtv2

            # assign
            hnew[i,j] = htp1
            vnew[i,j,:] = [v1_tp1, v2_tp1]

        end
    end

    return hnew, vnew

end

function shallowWater2D(h0, v0, deltaX, deltaT, nTsteps)

    (Nx, Ny) = size(h0)
    h = zeros((Nx, Ny, nTsteps + 1))
    h[:,:,1] = h0

    vprev = v0
    for nT in 1:nTsteps
        
        hnew, vnew = shallowWater2DStep(h[:,:,nT], vprev, deltaX, deltaT)
        # display(Plots.heatmap(hnew))

        h[:,:,nT + 1] = hnew
        vprev = vnew
    end

    return h, vprev

end

function main()

    # global parameters
    # domain is [-1,1]^2
    nTsteps = 100
    Nx = 100
    Ny = Nx
    deltaX = 2/Nx
    Tend = 1.0
    deltaT = Tend / nTsteps

    A = 1
    sigma = 5

    # initial conditions
    v0 = zeros((Nx, Ny, 2))
    h0 = ones((Nx, Ny))
    for i in 1:Nx
        for j in 1:Ny
            rsquared = (i - Nx/2)^2 + (j - Ny/2)^2
            h0[i,j] = A * exp(-rsquared / (sigma^2))
        end
    end

    h, vlast = shallowWater2D(h0, v0, deltaX, deltaT, nTsteps)

    # for i in 1:(nTsteps+1)
    #     display(Plots.heatmap(h[:,:,i]))
    # end

    anim = @Plots.animate for i in 1:(nTsteps+1)
        Plots.heatmap(h[:,:, i])
    end
    Plots.gif(anim, "output.gif")

end

main()

