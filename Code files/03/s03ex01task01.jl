# Computational Physics
# Set 03, Exercise 01, Task 01

using Plots
using Images
using Random
using Statistics
using JSON

"""
    takes 2D grid as input and creates a visual representation in PNG format.
    grid must contain values g_ij in {0,1,2,3} which are mapped to white, green, 
    red and black respectively. Black encodes a burnt-out tree.
"""
function gridToImg(lattice::Matrix{UInt8})
    img = similar(lattice, RGB)
    for i in 1:size(lattice, 1)
        for j in 1:size(lattice, 2)
            # empty
            if (lattice[i,j] == 0)
                img[i,j] = RGB(1.0, 1.0, 1.0)
            # tree
            elseif (lattice[i,j] == 1)
                img[i,j] = RGB(0.0, 1.0, 0.0)
            # burning tree
            elseif (lattice[i,j] == 2)
                img[i,j] = RGB(1.0, 0.0, 0.0)
            # burnt out tree
            elseif (lattice[i,j] == 3)
                img[i,j] = RGB(0.0, 0.0, 0.0)
            end 
        end
    end
    return img
end

"""
    Burns all the trees in the neighbourhood of the site indexed by i,j.
    Returns true if the sample percolates, i.e. if one of the newly burning sites 
    completes a spanning cluster.
"""
function burnNeighbourbood(lattice::Matrix{UInt8}, i, j)
    neighbouring_sites = [[i+1, j], [i-1, j], [i, j+ 1], [i, j-1]]
    for k in neighbouring_sites
        # make sure the neighbour actually exists
        if checkbounds(Bool, lattice, k...)
            # set 1 to 2, 0 to 0
            if lattice[k...] == 1
                lattice[k...] = 2
                # have we reached the other side of the grid?
                if k[1] == size(lattice, 1)
                    return true
                end
            end
        end
    end
    return false
end

"""
    Runs the simulation. First row is set on fire, then at each timestep, the 
    neighbours are set on fire as well. The originally burning trees are burnt out,
    encoded by the color black.
    Function terminates when either all trees have been burnt (returns false) or 
    when a spanning cluster has taken shape (return true).
"""
function forestFire(lattice::Matrix{UInt8})
    # burn the first row
    for j in 1:size(lattice, 2)
        if lattice[1,j] == 1
            lattice[1,j] = 2
        end
    end

    it = 1
    # indicates whether there are still trees burning
    burnflag = Bool(true)
    while burnflag
        # println("starting iteration $it")
        burnflag = false
        for i in 1:min(it, size(lattice, 1))
            for j in 1:size(lattice, 2)
                if lattice[i,j] == 2
                    # fires still raging
                    burnflag = true
                    # search and burn the neigbourhood
                    # return value indicates whether we've completed a spanning cluster
                    percolating = burnNeighbourbood(lattice, i,j)
                    if percolating
                        return 1, it + 1
                    end
                    # burn out the burning trees
                    lattice[i,j] = 3
                end
            end
        end
        it += 1
    end
    return 0, it
end


"""
    For a given lattice in the correct format, determines whether a percolating cluster
    has occured by checking whether the last row contains any burnt trees
    (i.e. sites encoded by 2 or 3).
"""
function checkPercolation(lattice::Matrix{UInt8})
    dim1 = size(lattice, 1)
    for j in 1:size(lattice, 2)
        site_int = lattice[dim1, j]
        if site_int == 2 || site_int == 3
            return true
        end
    end
    return false
end

"""
    Populates the forest with trees. Each site is populated randomly according
    to the probability p.
    :param lattice: empty grid of trees
    :param p: probability p ∈ [0,1] encoding the probability for a tree to populate a site
    :return : populated grid
"""
function populateForest(lattice::Matrix{UInt8}, p)
    for i in 1:size(lattice, 1)
        for j in 1:size(lattice, 2)
            # 1 if random number is smaller than prob, 0 if it is larger equal
            lattice[i,j] = Int8(rand(Float64) < p)
        end
    end
    return lattice
end

function runMultipleSims(grid_lengths, probabilities, no_sims)
    # result array included (for each probability and grid size) the probability 
    # for a spanning cluster and the average shortest path length

    results = zeros((size(grid_lengths, 1), size(probabilities, 1), 2))
    percolating, lp = 0,0

    for (ii, L) in enumerate(grid_lengths)
        println("grid length $L")
        for (jj, p) in enumerate(probabilities)
            println("probability $p")
            results_pL = zeros((2, no_sims))
            for n in 1:no_sims
                grid = zeros(UInt8, L, L)
                grid = populateForest(grid, p)

                # run simulation
                percolating, lp = forestFire(grid)
                results_pL[:,n] = [percolating, lp]
                # check for percolation
                # percolating = checkPercolation(grid)
            end
            # calculate averages
            results[ii, jj, 1] = mean(results_pL[1,:])
            results[ii, jj, 2] = mean(results_pL[2,:])
        end
    end
    return results
end

function main()
    

    # plot the result
    # img2 = gridToImg(grid)
    # savefig(heatmap(img2), "forest_end.png")
end

# global parameters
probabilities = range(0.5, 0.65; length=40)
no_sims = 1000
grid_lengths = [200, 300]

results = runMultipleSims(grid_lengths, probabilities, no_sims)
D = Dict("grid lengths" => grid_lengths, "probabilities" => probabilities, "percolation probability" => results[:,:,1],
         "percolation length" => results[:,:,2])

# pass data as a json string (how it shall be displayed in a file)
stringdata = JSON.json(D)

# write the file with the stringdata variable information
open("task01results.json", "w") do f
        write(f, stringdata)
     end

# Plots.plot()
for (ii, L) in enumerate(grid_lengths)
    Plots.plot!(probabilities, results[ii, :, 1], 
                title="Spanning Cluster Probability", xlabel="pₜ", ylabel="pₔ", 
                label="lattice length $L", legend=:topleft)
end
savefig("s03ex01_probs.png")