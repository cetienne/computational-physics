# Computational Physics
# Set 07, Exercise 2

# Hard spheres in a 3D box
# Author(s): Etienne Corminboeuf

using Random
import Statistics as st
import PlotlyJS as plt


##############
### Task 1 ###
##############
function distance(r1, r2)
    
    return sqrt((r1[1]- r2[1])^2 + (r1[2]- r2[2])^2 + (r1[3]- r2[3])^2)
end

"""
    Fills a box of edge length L with n hard spheres of radius R. Box has dimensions [0,L]^3
"""
function fillBox(L, n, R)

    positions = []
    
    
    for i in 1:n
        overlap = true
        cnt = 0
        while overlap
            if cnt > 200
                error("Timeout. Could not fill box of size $L with $n spheres of radius $R.")
            end
        
            overlap = false
            # randomly sample a point in the box
            ri = zeros(3)
            rand!(ri) .* L
            # check whether this sphere overlaps with any other spheres
            for r in positions
                if distance(r, ri) <= R
                    overlap = true
                    break
                end
            end
            if !overlap
                push!(positions, ri)
            end
            cnt += 1
        end
    end

    return positions
end

function averageDistanceSingle(positions)
    n = size(positions, 1)
    prefac = 2/(n*(n-1))

    davg_k = 0
    for j in 1:n
        for i in 1:j
            davg_k += distance(positions[i], positions[j])
        end
    end
    return prefac * davg_k

end

function averageDistanceHardSpheres(L, n, M, R)
    davgs = []
    for i in 1:M
        positions = fillBox(L, n, R)
        davg_sgnl = averageDistanceSingle(positions)
        # println(davg_sgnl)
        push!(davgs, davg_sgnl)
    end
    davg = st.mean(davgs)
    stdev = st.std(davgs)

    return davg, stdev
end

##############
### Task 2 ###
##############

function calculateDistances(Ms, L, ns, R)
    #Ms = 20:10:200
    #L = 5
    #ns = 10:2:40
    #R = 0.3

    davgs = zeros(size(Ms, 1), size(ns, 1))
    # traces = []
    
    for (j, n) in enumerate(ns)
        println("working with n=$n particles")
        for (i,M) in enumerate(Ms)
            davg, dstdev = averageDistanceHardSpheres(L, n, M, R)
            davgs[i,j] = davg
        end
        # plt.plot!(convplot, Ms, davgs, xaxis_type="log", yaxis_type="log", label="n=$n")
    end
    return davgs
end

function plotConvergence(Ms, L, ns, R, davgs)
    
    layout = plt.Layout(xaxis_type="log", yaxis_type="log", 
                        xaxis_title="M", yaxis_title="d_avg",
                        title="Average Distance, L=$L, "
                )
    traces = []

    for (j, n) in enumerate(ns)
        trace = plt.scatter(;x=Ms, y=davgs[:,j], mode="line", name="n=$n")
        push!(traces, trace)
        #plt.plot(trace, layout)
    end
    # println(traces)

    convplot = plt.plot([trace for trace in traces], layout)
    plt.savefig(convplot, "convplot_hardspheres.html")
    # plt.plot(Ms, davgs, xaxis_type="log", yaxis_type="log")
end

