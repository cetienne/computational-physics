# Computational Physics
# Set 07, Exercise 1

# Importance Sampling
# Author(s): Etienne Corminboeuf
using Random
import Plots as plt
import Polynomials as poly

global a = 0
global b = 1

function f(x)
    return exp(-x^2)
end

function g(x)
    return exp(-x) / (exp(-a) - exp(-b))
end

function G(x)
    return (exp(-a) - exp(-x))/(exp(-a) - exp(-b))
end

function Ginv(r)
    return log(r - exp(-a)/(exp(-b) - exp(-a)))
end

##############
### Task 1 ###
##############

"""
    npoints: number of sampling points 
    a: lower bound of integration domain
    b: upper bound of integration domain
"""
function uniformSamplingMC(npoints, f)

    # sample random points
    spoints = zeros(npoints)
    rand!(spoints) 
    # scale them to our integration bounds
    spoints = spoints *(b-a) .+ a

    # calulate the integral approximation
    I = sum(@.f(spoints)) * 1/(npoints)
    return I
end

function importanceSamplingMC(npoints, G, Ginv, f, g)

    # sample random points
    spoints = zeros(npoints)
    rand!(spoints) 
    # scale them to our integration bounds
    spoints = spoints *(G(b)-G(a)) .+ G(a)

    integrand(r) =  f(Ginv(r)) / g(Ginv(r))

    # calulate the integral approximation
    I = sum(@.integrand(spoints)) * 1/npoints

    return I
end

function plotConvergence(ns, G, Ginv, f, g)
    impVals = zeros(size(ns, 1))
    uniVals = zeros(size(ns, 1))

    # approximate the integral for a given number of sample points
    for (i,n) in enumerate(ns)
        impVals[i] = importanceSamplingMC(n, G, Ginv, f, g)
        uniVals[i] = uniformSamplingMC(n, f)
    end

    #traceImp = plt.scatter(ns, impVals, marker="line", lab="importanceSampling")
    # traceUni = plt.scatter(ns, uniVals, marker="line", lab="uniformSampling")

    plt.plot(ns, [impVals, uniVals], lab= ["importance" "uniform"])

    # plot errors
    nRef = 100000
    refValImp = importanceSamplingMC(nRef, G, Ginv, f, g)
    refValUni = uniformSamplingMC(nRef, f)

    # calculate errors wrt to value with big n
    errImp = @.abs(impVals .- refValImp)
    errUni = @.abs(uniVals .- refValUni)

    # calculate order of Convergence
    nslog = @.log(ns)
    errImplog = @.log(errImp)
    errUnilog = @.log(errUni)
    convPolyImp = poly.fit(nslog, errImplog, 1)
    convPolyUni = poly.fit(nslog, errUnilog, 1)
    println(convPolyImp)
    println(convPolyUni)
    orderImp = -poly.coeffs(convPolyImp)[2]
    orderUni = -poly.coeffs(convPolyUni)[2]

    fitImp = @.exp((@.convPolyImp(@.log(ns))))
    fitUni = @.exp((@.convPolyUni(@.log(ns))))

    plt.plot(ns, [errImp, errUni, fitImp , fitUni], lab=["importance" "uniform" "p=$orderImp" "p=$orderUni"], 
            title="Error Convergence", xaxis=:log, yaxis=:log)

end