# Computational Physics
# Set 06, Exercise 06

# Author(s): Etienne Corminboeuf

# HPP Gas Model


function HPPCollision(lattice::Array{Bool, 3})
    (Lx, Ly, Lz) = size(lattice)
    if Lz != 4
        error("invalid lattice given. dimension 3 must be of size 4!")
    end
    collisionLattice = Array{Bool,3}(undef, Lx, Ly, 4)

    for i in 1:Lx
        for j in 1:Ly
            # horizontal collision
            if lattice[i,j,:] == [1,0,1,0]
                collisionLattice[i,j,:] .= [0,1,0,1]
            # vertical collision
            elseif lattice[i,j,:] == [0,1,0,1]
                collisionLattice[i,j,:] .= [1,0,1,0]
            # no collision
            else
                collisionLattice[i,j,:] .= lattice[i,j,:]
            end
        end
    end

    return collisionLattice

end

function HPPPropagation(lattice::Array{Bool, 3})
    (Lx, Ly, Lz) = size(lattice)
    if Lz != 4
        error("invalid lattice given. dimension 3 must be of size 4!")
    end
    propagationLattice = Array{Bool,3}(undef, Lx, Ly, 4)
    for i in 1:Lx
        for j in 1:Ly
            # horizontal collision has happened, we propagate the particles outwards (no crossing site)
            if lattice[i,j,:] == [1,0,1,0]
                propagationLattice[i-1,j,3] = 1
                propagationLattice[i+1,j,1] = 1
                propagationLattice[i, j+1, 2] = 0
                propagationLattice[i, j-1, 4] = 0
            # vertical collision has happened, we propagate the particles outwards (no crossing sites)
            elseif lattice[i,j,:] == [0,1,0,1]
                propagationLattice[i,j-1,2] = 1
                propagationLattice[i,j+1,4] = 1
            # no collision
            else
                propagationLattice[i-1,j,3] = lattice[i,j,3]
                propagationLattice[i, j+1, 2] = lattice[i,j,2]
                propagationLattice[i+1, j, 1] = lattice[i,j,1]
                propagationLattice[i, j-1, 4] = lattice[i,j,4]
            end
        end
    end

end


function main()

end