# ComputationalPhysics
# Etienne Corminboeuf

import Plots as pl

function populateRoad(road, p, boundaryCondition)
    for j in 1:size(road, 1)
        # 1 if random number is smaller than prob, 0 if it is larger equal
        road[j] = Int8(rand(Float64) < p)
    end

    if boundaryCondition == "Dirichlet"
        road[1] = 0
        road[size(road, 1)] = 0
    elseif boundaryCondition == "Periodic"
        road[1] = road[size(road, 1)]
    end

end

function runTrafficSimulation(road0::Array{Int8, 1}, boundaryCondition, nsims)
    L = size(road0, 1)
    road = Matrix{Int8}(undef, nsims + 1, L)
    road[1,:] = road0 
    
    for i in 1:nsims
        # current evolution of road
        road_curr = zeros(Int8, L)

        # check lower bound
        if boundaryCondition == "Dirichlet"
            loc = "0" * string(road[i,1]) * string(road[i,2])

            # account for julia starting count at 1
            dec_rule = parse(Int, loc, base=2) + 1
            road_curr[1] = rulebook[dec_rule]
        elseif boundaryCondition == "Periodic"
            loc = string(road[i, L-1]) * string(road[i, 1]) * string(road[i, 2])
            road_curr[1] = road[i, L]
        end
        
        
        for j in 2:(L-1)
            loc = string(road[i, j-1]) * string(road[i, j]) * string(road[i, j+1])
            # account for julia starting count at 1
            dec_rule = parse(Int, loc, base=2) + 1
            road_curr[j] = rulebook[dec_rule]
        end

        # check upper bound
        if boundaryCondition == "Dirichlet"
            loc = string(road[i, L-1]) * string(road[i, L]) * "0"
        elseif boundaryCondition == "Periodic"
            loc = string(road[i, L-1]) * string(road[i, L]) * string(road[i, 2])
        end
        dec_rule = parse(Int, loc, base=2) + 1
        road_curr[L] = rulebook[dec_rule]
        # add road at current timestep to collection of roads
        road[i+1,:] = road_curr
        #push!(road, road_curr)
    end

    return road

end

function createRulebook(n)
    bs = bitstring(n)
    lbs = length(bs)
    rstart = lbs - 8
    rulebook = zeros(Int8, 8)
    # rulebook = bs[lbs-8:lbs]
    for i in lbs-7:lbs
        rulebook[i + 8 - lbs] = parse(Int, bs[i])
    end
    return rulebook
end

# Exercise 1: 1D automata
# Dirichlet boundary conditions: edges fixed at 0

p = 0.71
L = 100
nsims = 120

# the given rulebook, each entry corresponds to the local situation
# given by its index (starting from 0) in binary representation
nrule = 184
rulebook = createRulebook(nrule)
# rulebook = collect([0,0,0,1,1,1,0,1])

road0 = zeros(Int8, L)

# Dirichlet boundary conditions
boundaryCondition = "Dirichlet"
populateRoad(road0, p, boundaryCondition)
road = runTrafficSimulation(road0, boundaryCondition, nsims)
gr()
heatmap(road)
savefig("hm_dirichlet.png")

# Periodic boundary conditions
boundaryCondition = "Periodic"
populateRoad(road0, p, boundaryCondition)
road = runTrafficSimulation(road0, boundaryCondition, nsims)
heatmap(road)
savefig("hm_periodic.png")

# phase transistion for rule 184 occurs approximately at p=0.7

