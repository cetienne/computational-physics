# Computational Physics
# Exercise Set 06

# Exercise 2
# Author(s): Etienne Corminboeuf

using Plots
import Plots as pl

"""
    Populates the grid with points. Each site is populated randomly according
    to the probability p.
    :param lattice: empty grid of sites
    :param p: probability p ∈ [0,1] encoding the probability for a site to be populated
    :return : populated grid
"""
function populateForest(lattice::Matrix{UInt8}, p)
    for i in 1:size(lattice, 1)
        for j in 1:size(lattice, 2)
            # 1 if random number is smaller than prob, 0 if it is larger equal
            lattice[i,j] = Int8(rand(Float64) < p)
        end
    end
    return lattice
end

function atDirichlet(lattice::Matrix{UInt8}, ind)
    if checkbounds(Bool, lattice, ind...)
        return lattice[ind...]
    else
        return 0
    end
end

function sumUpNeighbourhood(lattice::Matrix{UInt8}, ind, boundaryCondition)
    (i,j) = ind
    xrange = i-1:i+1
    yrange = j-1:j+1
    sum = 0
    # sum over all points in the neighborhood, including the point itself
    for k in xrange
        for l in yrange
            if boundaryCondition == "Dirichlet"
                sum += atDirichlet(lattice, (k,l))
            else
                error("unknown boundary condition identifier!")
            end
        end
    end
    # subtract the point itself
    sum -= atDirichlet(lattice, (i,j))
    return sum

end

function GOLTimestep(lattice::Matrix{UInt8}, ind, boundaryCondition)
    sumN = sumUpNeighbourhood(lattice, ind, boundaryCondition)
    # isolation
    if sumN == 0 || sumN == 1
        return 0
    # stay
    elseif sumN == 2
        return lattice[ind...]
    # birth
    elseif sumN == 3
        return 1
    # overpopulation
    elseif sumN > 3
        return 0
    end
end

function GameOfLife(lattice::Matrix{UInt8}, boundaryCondition, nsims)
    (Lx, Ly) = size(lattice)

    latticecollection = Array{UInt8, 3}(undef, Lx, Ly, nsims + 1)
    latticecollection[:,:,1] = lattice

    hm = pl.heatmap(latticecollection[:,:,1], title="iteration 0")
    pl.savefig(hm, "GOL_it0.png")
    for n in 1:nsims
        println("performing iteration no. $n")
        for i in 1:Lx
            for j in 1:Ly
                latticecollection[i,j, n+1] = GOLTimestep(latticecollection[:,:,n], (i,j), boundaryCondition)
            end
        end
        # hm = pl.heatmap(latticecollection[:,:,n+1], title="iteration $n")
        # pl.savefig(hm, "GOL_it$n.png")
    end
    return latticecollection
end

function createGif(latticecollection)
    nsims = size(latticecollection, 3)
    mygif = pl.@animate for s in 1:nsims
        pl.heatmap(latticecollection[:,:,s])
    end

    pl.gif(mygif, "stripes_game_of_life.gif", fps = 10)
end


function main()

    # user parameters
    boundaryCondition = "Dirichlet"
    Lx = 100
    Ly = 100
    nsims = 200
    p = 0.4

    # known example for debugging
    # "block"
    # L = 4
    # nsims = 10
    # boundaryCondition = "Dirichlet"
    # lattice = zeros(UInt8, L, L)
    # lattice[2:3, 2:3] .= 1

    # "beehive"
    # Lx = 5
    # Ly = 6
    # nsims = 10
    # boundaryCondition = "Dirichlet"
    # lattice = zeros(UInt8, Lx, Ly)
    # lattice[2,3] = 1
    # lattice[2,4] = 1
    # lattice[3, 2] = 1
    # lattice[3, 5] = 1
    # lattice[4, 3] = 1
    # lattice[4, 4] = 1

    # "blinker"
    #= Lx = 5
    Ly = 5
    nsims = 10
    lattice = zeros(UInt8, Lx, Ly)
    lattice[2:4,3] .= 1 =#

    # "glider"
    # Lx = 7
    # Ly = 7
    # nsims = 10
    # lattice = zeros(UInt8, Lx, Ly)
    # lattice[4, 2:4] .= 1
    # lattice[6, 3] = 1
    # lattice[5, 4] = 1

    lattice = zeros(UInt8, Lx, Ly)
    # lattice = populateForest(lattice, p)

    for i in 1:3:Lx
        lattice[i,:] .= 1
    end
    # lattice[Int64(Lx/2)-3: Int64(Lx/2)+ 3,Int64(Ly/2)-3:Int64(Ly/2)+3] .= 1

    pl.gr()

    latticecollection = GameOfLife(lattice, boundaryCondition, nsims)
    createGif(latticecollection)

end

main()