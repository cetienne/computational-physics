"""
    For a given pair of indeces, finds the closest occupied site. Does so by traversing the 
    lattice around (i,j) in a spiral manner.
    :param lattice: a NxM sized grid
    :return : indeces (k,l) of  next occupied site
"""
function findOccupiedSite(lattice::Matrix{UInt8}, i,j)
    if lattice[i,j] != 0
        return (i,j)
    end
    for d1 in 1:min(i, size(lattice, 1) - i)
        for d2 in 1:min(j, size(lattice, 1) - j)
            indsi = i-d1:i+d1
            indsj = j-d2:j+d2
            for k in indsi
                for l in indsj
                    if lattice[k,l] != 0
                        return (k,l)
                    end
                end
            end
        end
    end
    error("could not find occupied site on lattice!")
    
end

"""
    Counts the amount of sites with non-zero value in the given lattice.
    :return : N, number of non-zero sites
"""
function countOccupiedSites(lattice::Matrix{UInt8})
    cnt = 0
    for i in 1:size(lattice, 1)
        for j in 1:size(lattice, 2)
            if lattice[i,j] != 0
                cnt += 1
            end
        end
    end
    return cnt
end


function sandbox(lattice::Matrix{UInt8}, p)
    L = size(lattice, 1)
    i = floor(L/2)
    j = j

    (k,l) = findOccupiedSite(lattice, i, j)
    Rs = 2:2:L
    N = []
    for R in Rs
        r = Int64(R/2)
        NR = countOccupiedSites(lattice[k-r:k+r, l-r:l+r])
        push!(N, NR)
    end

    
end