# Computational Physics
# Set 05, Exercise 01
# Etienne Corminboeuf

using PlotlyJS: plot, savefig, scatter, Layout
using Polynomials
using Dates
using JSON

"""
    Populates the forest with trees. Each site is populated randomly according
    to the probability p.
    :param lattice: empty grid of trees
    :param p: probability p ∈ [0,1] encoding the probability for a tree to populate a site
    :return : populated grid
"""
function populateForest(lattice::Matrix{UInt8}, p)
    for i in 1:size(lattice, 1)
        for j in 1:size(lattice, 2)
            # 1 if random number is smaller than prob, 0 if it is larger equal
            lattice[i,j] = Int8(rand(Float64) < p)
        end
    end
    return lattice
end

"""
    For a given lattice in the correct format, determines whether a percolating cluster
    has occured by checking whether the last row contains any burnt trees
    (i.e. sites encoded by 2 or 3 (burnt out)).
"""
function checkPercolation(lattice::Matrix{UInt8})
    dim1 = size(lattice, 1)
    for j in 1:size(lattice, 2)
        site_int = lattice[dim1, j]
        if site_int == 2 || site_int == 3
            return true
        end
    end
    return false
end

"""
    Burns all the trees in the neighbourhood of the site indexed by i,j.
    Returns true if the sample percolates, i.e. if one of the newly burning sites 
    completes a spanning cluster.
"""
function burnNeighbourbood(lattice::Matrix{UInt8}, i, j)
    neighbouring_sites = [[i+1, j], [i-1, j], [i, j+ 1], [i, j-1]]
    for k in neighbouring_sites
        # make sure the neighbour actually exists
        if checkbounds(Bool, lattice, k...)
            # set 1 to 2, 0 to 0
            if lattice[k...] == 1
                lattice[k...] = 2
                # have we reached the other side of the grid?
                if k[1] == size(lattice, 1)
                    return true
                end
            end
        end
    end
    return false
end

"""
    Propagates an already started fire. At each timestep, the 
    neighbours are set on fire as well. The originally burning trees are burnt out,
    encoded by the color black.
    Function terminates when either all trees have been burnt (returns false) or 
    when a spanning cluster has taken shape (return true).
"""
function propagateFire(lattice::Matrix{UInt8})

    it = 1
    # indicates whether there are still trees burning
    burnflag = Bool(true)
    while burnflag
        # println("starting iteration $it")
        burnflag = false
        for i in 1:min(it, size(lattice, 1))
            for j in 1:size(lattice, 2)
                if lattice[i,j] == 2
                    # fires still raging
                    burnflag = true
                    # search and burn the neigbourhood
                    # return value indicates whether we've completed a spanning cluster
                    percolating = burnNeighbourbood(lattice, i,j)
                    if percolating
                        return true, it + 1, lattice
                    end
                    # burn out the burning trees
                    lattice[i,j] = 3
                end
            end
        end
        it += 1
    end
    return false, it, lattice
end

function delUnburntTrees(lattice::Matrix{UInt8})
    for i in 1:size(lattice, 1)
        for j in 1:size(lattice, 2)
            if lattice[i,j] == 1
                lattice[i,j] = 0
            elseif lattice[i,j] == 2
                lattice[i,j] = 3
            end
        end
    end
    return lattice
end

function findPercolatingCluster(lattice::Matrix{UInt8})
    # burn tree in first row and check for percolation
    for j in 1:size(lattice, 2)
        if lattice[1,j] == 1
            lattice[1,j] = 2

            perc, it, _ = propagateFire(lattice)
            if perc
                lattice = delUnburntTrees(lattice)
                # found a percolating cluster
                return true, lattice
            end
        end
    end
    # could not find a percolating cluster
    return false, lattice
end

function setUpSystem(L, p)
    perc = false
    lattice = zeros(UInt8, (L,L))
    while !perc
        println("(re-)generating forest")
        lattice = populateForest(lattice, p)
        perc, lattice = findPercolatingCluster(lattice)
    end
    return lattice
end

function checkOccupationSupergrid(lattice::Matrix{UInt8}, eps, L)
    dim_supergrid = ceil(L/eps)
    N = 0
    println("checkOccupationSupergrid: eps= $eps")
    for i in 1:eps
        # determine which indeces of the lattice are covered by the supergrid cell
        lower_ind = Int64(min((i - 1)*dim_supergrid + 1, L))
        upper_ind = Int64(min(i*dim_supergrid, L))
        inds_x =  lower_ind:1:upper_ind
        for j in 1:eps
            lower_ind = Int64( min((j - 1)*dim_supergrid + 1, L))
            upper_ind = Int64(min(j*dim_supergrid, L))
            inds_y = lower_ind:1:upper_ind

            sites = lattice[inds_x, inds_y]
            # if at least one site is occupied, then this will be non-zero
            occ = maximum(sites)
            if occ > 0
                N += 1
            end
        end
    end
    return N
end

"""
    Runs the box counting algorithm for the given lattice.
    Creates a log-log-plot of 1/eps vs N_eps.
    :param lattice:
    :param p: probability for site to be occupied
    :param create plot: bool, if true plots are created and saved.
    :return : fractal_dim
"""
function boxCounting(lattice::Matrix{UInt8}, p, create_plot)
    L = size(lattice, 1)
    # epsilon denotes how many cells in each dimension the supergrid consists of
    epsilons = 2:3:ceil(L/2)
    N_eps = []
    for eps in epsilons
        N = checkOccupationSupergrid(lattice, eps, L)
        push!(N_eps, N)
    end
    epsilons = collect(epsilons)
    epsilon_inverse = collect(1/(@.epsilons))
    epsilon_inverse = transpose(epsilon_inverse)[:,1]

    N_eps = collect(@.Int64(N_eps))
    # fit a polynomial to the data to determine the slope
    println(epsilon_inverse)
    println(N_eps)
    log_eps_inv = @.log(epsilon_inverse)
    print(log_eps_inv)
    pfit = fit(collect(@.log(epsilon_inverse)), collect(@.log(N_eps)), 1)

    fitfunc(x) = exp(coeffs(pfit)[1])*x^(coeffs(pfit)[2])
    fractal_dim = coeffs(pfit)[2]
    fractal_dim_round = round(fractal_dim, digits=2)
    
    if create_plot
        layout = Layout(xaxis_type="log", yaxis_type="log", 
                        xaxis_title="N_ϵ", yaxis_title="1/ϵ",
                        title="Fractal Dimension: Box Counting. p=$p, d_frac=$fractal_dim_round}"
                )

        domain = collect(range(minimum(epsilon_inverse), maximum(epsilon_inverse); length=200))

        trace1 = scatter(;x=epsilon_inverse, y=N_eps, mode="markers", name="data")
        trace2 = scatter(;x=domain, y=@.fitfunc(domain))

        pl = plot([trace1, trace2],
                    layout)

        fname = "box_counting_" * "L$L" * "_" * "p$p" * ".png"
        savefig(pl, fname)
    end

    return fractal_dim
end
"""
    For a given pair of indeces, finds the closest occupied site. Does so by traversing the 
    lattice around (i,j) in a spiral manner.
    :param lattice: a NxM sized grid
    :return : indeces (k,l) of  next occupied site
"""
function findOccupiedSite(lattice::Matrix{UInt8}, i,j)
    if lattice[i,j] != 0
        return (i,j)
    end
    for d1 in 1:min(i, size(lattice, 1) - i)
        for d2 in 1:min(j, size(lattice, 1) - j)
            indsi = i-d1:i+d1
            indsj = j-d2:j+d2
            for k in indsi
                for l in indsj
                    if lattice[k,l] != 0
                        return (k,l)
                    end
                end
            end
        end
    end
    error("could not find occupied site on lattice!")
    
end

"""
    Counts the amount of sites with non-zero value in the given lattice.
    :return : N, number of non-zero sites
"""
function countOccupiedSites(lattice::Matrix{UInt8})
    cnt = 0
    for i in 1:size(lattice, 1)
        for j in 1:size(lattice, 2)
            if lattice[i,j] != 0
                cnt += 1
            end
        end
    end
    return cnt
end

"""
    Computes the fractal dimension using the sandbox method.
    :param lattice: 
    :param p: probability for a site to be occupied (for plot labelling)
    :param create_plot: bool, if true then plots are created and saved
    :return : the fractal dimension for the structure in this lattice
"""
function sandbox(lattice::Matrix{UInt8}, p, create_plot)
    L = size(lattice, 1)
    i = Int64(floor(L/2))
    j = i

    (k,l) = findOccupiedSite(lattice, i, j)
    Rs = 2:2:min(L-i, L-j)
    N = []
    for R in Rs
        r = Int64(R/2)
        NR = countOccupiedSites(lattice[k-r:k+r, l-r:l+r])
        push!(N, NR)
    end

    pfit = fit(collect(@.log(Rs)), collect(@.log(N)), 1)

    fitfunc(x) = exp(coeffs(pfit)[1])*x^(coeffs(pfit)[2])
    fractal_dim = round(coeffs(pfit)[2], digits=2)
    
    if create_plot
        layout = Layout(xaxis_type="log", yaxis_type="log", 
                        xaxis_title="R", yaxis_title="N_R",
                        title="Fractal Dimension: Sandbox Method. p=$p, d_frac=$fractal_dim}"
                        )

        domain = collect(range(minimum(Rs), maximum(Rs); length=200))

        trace1 = scatter(;x=Rs, y=N, mode="markers", name="data")
        trace2 = scatter(;x=domain, y=@.fitfunc(domain))

        pl = plot([trace1, trace2],
        layout)

        fname = "sandbox" * "L$L" * "_" * "p$p" * ".png"
        savefig(pl, fname)
    end

    return fractal_dim
    
end


function main()
    Ls = [2000]
    no_sims = 50

    fractal_dimensions = Dict("box counting" => [], "sandbox" => [])

    for L in Ls
        
        for n in 1:no_sims
            println("running simulation $n of $no_sims")
            p = 0.6

            lattice = setUpSystem(L, p)
            lattice = delUnburntTrees(lattice)

            fdim = boxCounting(lattice, p, true)
            push!(fractal_dimensions["box counting"], fdim)
            fdim = sandbox(lattice, p, true)
            push!(fractal_dimensions["sandbox"], fdim)
        end

        
    end

    # pass data as a json string (how it shall be displayed in a file)
    stringdata = JSON.json(fractal_dimensions)

    ts = now()
    # write the file with the stringdata variable information
    open("fractaldims_$ts.json", "w") do f
            write(f, stringdata)
    end
    
end

main()
# todo: add sandbox method
