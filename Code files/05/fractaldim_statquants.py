import json
from typing import ByteString
import numpy as np

with open("/home/cetienne/Documents/Studium/MSc Physics/HS21/Computational Physics/computational-physics/Code files/05/fractaldims_2021-10-28T16:15:14.725.json") as f:
    d = json.load(f)

bc = d["box counting"]
sb = d["sandbox"]

bc_mean = np.mean(bc)
sb_mean = np.mean(sb)

print("####")
print("Box Counting")
print("mean: ", np.mean(bc))
print("median: ", np.median(bc))
print("Std: ", np.std(bc))

print("####")
print("Sandbox")
print("mean: ", np.mean(sb))
print("median: ", np.median(sb))
print("Std: ", np.std(sb))