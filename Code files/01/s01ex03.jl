# Set 01, Exercise 03
# Etienne Corminboeuf

using Random
using Plots

# create two random variables x,y
n = 1000
R = 1

rng = MersenneTwister(5472)
x = zeros(n)
rand!(rng, x)

rng = MersenneTwister(7483)
z = zeros(n)
rand!(rng, z)

xprime = x.*@. sin(z/R)
zprime = R * @.cos(z./R)

yprime = @.sqrt(R^2 - xprime^2 - zprime^2)

scatter3d(xprime, yprime, zprime)