# Set 01, Exercise 5
# Etienne Corminboeuf

# The D*-discrepancy

using Random
using HaltonSequences
using Formatting
using Statistics
include("s01ex01.jl")

"""
    Calculate the term in the D*-discrepancy formula for a fixed v.
    x = Array(d, N)
"""
function discrepancy(x, v)
    d, N = size(x)

    # first term
    noPointsInCube = 0
    for i in 1:N
        # check whether point within cube spanned by v
        incube = x[:,i] .<= v
        noPointsInCube += Int64(prod(incube))
    end
    pic_norm = 1/N * noPointsInCube

    # second term
    volCube = prod(v)

    return abs(pic_norm - volCube)

end

"""
    Calculate the D*-discrepancy for a sequence of points x = [x₁, x₂, …, xₙ].
    The supremum is taken over a randomly generated sample of vectors v.
    :param x: sequence of points
    :param d: dimension of problem, xᵢ ∈ Rᵈ.
"""
function Dstar(x, d, n=1000)
    # number of vectors
    rng = MersenneTwister(2134)
    v = zeros(d, n)
    rand!(rng, v)

    sup = 0
    for i in 1:n
        discr = discrepancy(x, v[:,i])
        if discr > sup
            sup = discr
        end
    end

    return sup

end

# define global parameters
d = 4
lenSeq = 10000
# generate random numbers with the CRNG from ex. 1
crngRands = zeros(d, lenSeq)
haltonRands = zeros(d, lenSeq)
for ii in 1:d
    crngRands[ii,:] = CRNG(lenSeq, rand(1:27))
    haltonRands[ii,:] = Halton(29, start=rand(1:3000), length=lenSeq)
end

# crngDstar = Dstar(crngRands, d)
# haltonDstar = Dstar(haltonRands, d)

# lets do a few iterations and look at statistical quantities
numit = 50

crngDstar = zeros(numit)
haltonDstar = zeros(numit)

for jj in 1:numit
    crngDstar[jj] = Dstar(crngRands, d)
    haltonDstar[jj] = Dstar(haltonRands, d)
end

printfmt("#############")
printfmt("D*")
printfmt("number of iterations: {:d}", numit)
printfmt("CRNG: ")
printfmt("Mean: {:f}", mean(crngDstar))
printfmt("Std: {:s}", std(crngDstar))
printfmt("")
printfmt("Halton: ")
printfmt("Mean: {:f}", mean(haltonDstar))
printfmt("Std: {:s}", std(haltonDstar))
printfmt("#########")

# lets look at convergence
# i want to simulate to a maximum of 20000 random vectors
tmp = range(2, 10, length=30)
lenseqs = @.Int64(@.floor(@.exp(tmp)))

crngDstar = zeros(size(lenseqs))
haltonDstar = zeros(size(lenseqs))

for jj in 1:size(lenseqs)[1]
    #println(lenseqs[jj])
    #crngRands = zeros(d, lenseqs[jj])
    #haltonRands = zeros(d, lenseqs[jj])
    #for ii in 1:d
    #    crngRands[ii,:] = CRNG(lenseqs[jj], rand(1:27))
    #    haltonRands[ii,:] = Halton(29, start=rand(1:3000), length=lenseqs[jj])
    #end
    
    crngDstar[jj] = Dstar(crngRands, d, lenseqs[jj])
    haltonDstar[jj] = Dstar(haltonRands, d, lenseqs[jj])
end

plot(lenseqs, crngDstar, xlabel="no. of vectors v", ylabel="D*", title="D* for CRNG")
plot(lenseqs, haltonDstar, xlabel="no. of vectors v", ylabel="D*", title="D* for Halton")

