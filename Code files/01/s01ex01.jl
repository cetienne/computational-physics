# Sheet 01, exercise 01
# Etienne Corminboeuf

c = 3
p = 31

"""
    CRNG(n, x0, c=3, p=31)
Creates and returns the first n numbers in the sequence.
x_i = (c*x_{i-1}) mod p
"""
function CRNG(n, x0, c=3, p=31)
    
    # allocate space
    rn = zeros(n) 
    rn[1] = x0
    for i in 2:n
        rn[i] = c*rn[i-1] % p
    end
    return rn
end

# generate the random numbers
n = 100
seed = 20
rands = CRNG(n, seed)

using Plots
# Task 1

display(scatter(rands[1:(n-1)], rands[2:n]))

# Task 2
display(scatter3d(rands[1:(n-2)], rands[2:(n-1)], rands[3:n]))

# Task 3

n = 100
seed = 197
rands = CRNG(n, seed, 7, 283)

display(scatter(rands[1:(n-1)], rands[2:n]))

display(scatter3d(rands[1:(n-2)], rands[2:(n-1)], rands[3:n]))

