# Set 01, Exercise 04
# Etienne Corminboeuf

# global variables
n = 1000
axis_maj = 0.5
axis_min = 0.2

@. function ellipse(x, a=axis_maj, b=axis_min)
    return x[1]^2/a^2 + x[2]^2/b^2
end

@. function isWithinEllipse(x)
    return ellipse(x) <= 1
end

# generate random numbers
rng = MersenneTwister(8393)
x = zeros((n, 2))
rand!(rng, x)
# recenter them around the origin
x = x .- 0.5
# and scale them to within a box with sidelengths equal to 
# twice the major semi-axis
x = x *2 .*axis_maj
ellrandx = Array{Float64,1}(undef, 2)
ellrandy = Array{Float64,1}(undef, 2)

for i in range(1,size(x)[1], step=1)
    println(x[i,:])
    if ellipse(x[i,:]) <= 1
        push!(ellrandx, x[i,1])
        push!(ellrandy, x[i,2])
    end
end

println(ellrandy)
using Plots
scatter(ellrandx, ellrandy)