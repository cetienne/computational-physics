# Computational Physics
# Set 04, Exercise 01
# Tasks 1 and 2
# Author: Etienne Corminboeuf

using Images
using Random
using PlotlyJS
using DataStructures

function gridToImg(lattice::Matrix{UInt})
    img = similar(lattice, RGB)
    for i in 1:size(lattice, 1)
        for j in 1:size(lattice, 2)
            # empty
            if (lattice[i,j] == 0)
                img[i,j] = RGB(1.0, 1.0, 1.0)
            # tree
            elseif (lattice[i,j] == 1)
                img[i,j] = RGB(0.0, 1.0, 0.0)
            # burning tree
            elseif (lattice[i,j] == 2)
                img[i,j] = RGB(1.0, 0.0, 0.0)
            # burnt out tree
            elseif (lattice[i,j] == 3)
                img[i,j] = RGB(0.0, 0.0, 0.0)
            end 
        end
    end
    return img
end


"""
    Populates the forest with trees. Each site is populated randomly according
    to the probability p.
    :param lattice: empty grid of trees
    :param p: probability p ∈ [0,1] encoding the probability for a tree to populate a site
    :return : populated grid
"""
function populateForest(lattice::Matrix{UInt}, p)
    for i in 1:size(lattice, 1)
        for j in 1:size(lattice, 2)
            # 1 if random number is smaller than prob, 0 if it is larger equal
            lattice[i,j] = Int8(rand(Float64) < p)
        end
    end
    return lattice
end

"""
    Checks whether the matrix element at the position given by (i,j) equals 
    to the given value. If the position is out of bounds, then it is treated as 
    an empty field (encoded by 0).
"""
function arrelequal(lattice::Matrix{UInt}, i, j, value)
    if checkbounds(Bool, lattice, i, j)
        return (lattice[i,j] == value)
    elseif value == 0
        return true
    else
        return false
    end
end

function arrarrequal(lattice::Matrix{UInt},i ,j ,k ,l)
    atmp = checkbounds(Bool, lattice, i, j)
    btmp = checkbounds(Bool, lattice, k, l)
    if atmp && btmp
        return lattice[i,j] == lattice[k,l]
    elseif atmp
        return atmp == 0
    elseif btmp
        return btmp == 0
    # none of them within bounds: they are both counted as empty, i.e. zero
    else
        return true
    end
end

"""
    Returns the cluster size M(k) and the index k.
    As M(k) might be a negative number, this function recursively finds
    the first entry M(k) which is positive, thus actually denotes the cluster size.
"""
function recursiveFind(M, k)
    println("entered recursiveFind")
    ll = M[k]
    println("M $ll")
    if M[k] >= 0
        return M[k], k
    else
         return recursiveFind(M, -1*M[k])
    end
end

"""
    Runs the Hoshen-Kopelman algorithm on given lattice. Returns the cluster sizes in an array and the indexed lattice.
"""
function HoshenKopelman(lgrid::Matrix{UInt})
    lattice = copy(lgrid)

    dim = size(lattice, 1)
    if dim != size(lattice, 2)
        error("dimension mismatch. lattice must be square lattice!")
    end

    # cluster label
    k = 2
    # cluster size
    M = zeros(Int64, 1)

    for i in 1:dim
        # println("iterating over row $i")
        for j in 1:dim
            # println(M)
            # Plots.heatmap(lattice)
            if lattice[i,j] == 1
                if arrelequal(lattice, i, j-1, 0) && arrelequal(lattice, i-1, j, 0)
                    # println("entered first clause")
                    lattice[i,j] = k
                    push!(M, 1)
                    k += 1
                elseif !arrelequal(lattice, i, j-1, 0) && arrelequal(lattice, i-1, j, 0)
                    # println("entered second clause")
                    lattice[i,j] = lattice[i, j-1]
                    # println(lattice[i, j-1])
                    ind = recursiveFind(M, lattice[i,j-1])[2]
                    M[ind] += 1
                elseif arrelequal(lattice, i, j-1, 0) && !arrelequal(lattice, i-1, j, 0)
                    # println("entered 3rd clause")
                    lattice[i,j] = lattice[i-1, j]
                    # println(lattice[i-1, j])
                    ind = recursiveFind(M, lattice[i-1, j])[2]
                    M[ind] += 1
                elseif arrarrequal(lattice, i, j-1, i-1, j)
                    # println("entered fourth clause")
                    lattice[i,j] = lattice[i-1, j]
                    ind = recursiveFind(M, lattice[i-1, j])[2]
                    M[ind] += 1
                    # no need to check boundaries. if i-1 is out of bounds then it will either be 
                # caught by clause 1 or 2
                else
                    # println("entered last clause")
                    lattice[i,j] = lattice[i-1, j]
                    println(lattice[i-1, j])
                    println(lattice[i, j-1])
                    (aboveval, aboveind) = recursiveFind(M, lattice[i-1, j])
                    println(aboveind)
                    (leftval, leftind) = recursiveFind(M, lattice[i, j-1])
                    println(leftind)
                    # make sure we don't merge two clusters that have different label but are
                    # already connected in the mass function M
                    if aboveind == leftind
                        println("aboveind==leftind")
                        M[aboveind] += 1 
                        # M[lattice[i, j-1]] 
                    else
                        M[aboveind] += 1 + leftval
                        M[lattice[i, j-1]] = -1*Int64(aboveind)
                    end 
                    
                end
            end
        end
    end
    return lattice, M

end

function runSim(L, p)
    lattice = zeros(UInt, (L,L))
    populateForest(lattice, p)

    # img2 = gridToImg(lattice)
    # savefig(heatmap(img2), "forest_end_ts.png")

    lgrid, M = HoshenKopelman(lattice)

    return lgrid, M
end

function main()

    # define global parameters
    Ls = [100]
    # ps = 0.3:0.1:0.8
    ps = [0.5]
    nsims = 50

    for L in Ls
        for p in ps
            println("Running HoshenKopelman for p=$p at L=$L")
            pcnt = counter([])
            Mtot = []
            for i in 1:nsims
                println("Interation $i of $nsims")
                lgrid, M = runSim(L, p)
                push!(Mtot, M)
                cnt = counter(M)
                merge!(pcnt, cnt)
            end
            flatM = vcat(Mtot)
            hist = histogram(x=flatM, xbins_start=0, xbins_end=maximum(flatM))
            # hist = histogram(x=pcnt, xbins_start=0, xbins_end=maximum(pcnt))
            savefig(plot(hist), "hist_p$p.png")
        end
    end

end


main()
# L = 10
# p = 0.5
# lattice = zeros(UInt, (L,L))
# populateForest(lattice, p)

# # img2 = gridToImg(lattice)
# # savefig(heatmap(img2), "forest_end_ts.png")

# lgrid, M = HoshenKopelman(lattice)

# println(M)

# pl = plot(heatmap(z=lgrid, text=lgrid))
# savefig(pl, "dbug.png")
# hist = histogram(x=M, xbins_start=0, xbins_end=maximum(M))
# plot(hist)
# println(hist)
