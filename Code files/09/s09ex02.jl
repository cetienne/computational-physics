# Computational Physics
# Set 9, Exercise 02

# CTCS Scheme

# Author(s): Etienne Corminboeuf

import LinearAlgebra as la
import Plots as plt
using Plots


function CTCS1D(lattice0, no_tsteps, c)

    N = size(lattice0, 1)

    # matrix for evolution
    # ( 0 c 0 0 ... -c )
    # ( c 0 -c 0 ... 0 )
    # ...
    # (-c 0 0 ...  c 0)
    du = c * ones(N-1)
    dl = -du
    d = zeros(N)
    Mc = la.Tridiagonal(dl, d, du)
    Mc = Array(Mc)
    Mc[1,N] = -c
    Mc[N,1] = c

    lattice_nm1 = copy(lattice0)
    # first timestep using FTCS
    lattice_n = lattice0 - 0.5*Mc * lattice0

    # other timesteps using CTCS
    lattice_np1 = zeros(N)
    lattices = [lattice0, lattice_n]

    for n in 1:no_tsteps
        lattice_np1 = lattice_nm1 .- Mc * lattice_n

        push!(lattices, lattice_np1)

        # if n%10 == 0
        #     println(lattice_np1)
        # end

        # update the lattices
        lattice_nm1 = copy(lattice_n)
        lattice_n = copy(lattice_np1)

    end

    return lattice_np1,lattices

end

function main()
    
    # domain [0,1]
    domstart = 0
    domend = 1
    u = 1
    Δx = 0.005

    N = Int64(floor(1/Δx))

    domain = range(domstart, domend; length=N)

    T = 1/u
    no_tsteps = 200
    Δt = T / no_tsteps

    c = u *Δt /Δx
    
    # initial condition: square pulse
    #= lattice0 = ones(N)
    for i in 1:N
        x = domstart + i * Δx
        if abs(x - 0.5) > 0.2
            lattice0[i] = 0
        end
    end =#

    # initial condition: Gaussian
    lattice0 = ones(N)
    sigma = 0.1
    for i in 1:N
        x = domstart + i * Δx
        lattice0[i] = 1/sqrt(2*pi*sigma) * exp(-(x-0.5)^2/(2*sigma))
    end

    lattice_fin, lattices = CTCS1D(lattice0, no_tsteps, c)

    mygif = plt.@animate for s in 1:(N+1)
        plt.plot(domain, lattices[s])
    end
    plt.gif(mygif, "evol_gaussian.gif", fps = 10)

        
    plt.plot(domain, [lattice0, lattice_fin], lab=["Phi0" "PhiN"])

    #println(lattice0)
    #println(lattice_fin)

end



