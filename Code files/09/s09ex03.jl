# Computational Physics
# Set 09, Exercise 03

# Poissons equation

# Author(s): Etienne Corminboeuf

import Plots as plt
import LinearAlgebra as la


function deallocate(arrs)
    for arr in arrs
        arr = Nothing
    end

    return
end

function finiteDifferencePoisson(N, h)
    
    du = ones(Int8, N^2-1)
    dl = du
    d = -4*ones(Int8, N^2)
    A = la.Tridiagonal{Int8}(dl, d, du)

   deallocate([du, dl, d])

    A = Array(A)
    for i in 1:(N-1)
        A[i*N,i*N+1] = 0
    end
    itend = N^2-N
    for i in 1:itend
        # TODO: add offdiagonal ones
        A[i, N+i] = 1
    end
    # create a symmetric matrix from the skelethon we have in A
    Asymm = la.Symmetric(A, :U)
    deallocate([A])
    Asymm = Array{Float64}(Asymm)

    B = -h^2 * ones((N^2, 1))

    la.LAPACK.gesv!(Asymm, B)

    u = reshape(B, (N, N))

    deallocate([B])

    return u

end

function exactPoisson(N)
    coordinate_lattice = zeros((N,N, 2))
    u = zeros((N,N))
    for i in 1:N
        for j in 1:N
            coordinate_lattice[i,j,1] = (i - 1 - N/2) * 2/N + 1/N  
            # + 1/N term is to place all points within (-1,1), not on boundary
            coordinate_lattice[i,j,2] = (j - 1 - N/2) * 2/N + 1/N

            # directly evaluate the poissons equation solution
            (x, y) = coordinate_lattice[i,j,:]
            u[i,j] += (1 - x^2)/2
            ks = 1:2:100
            summands = zeros((size(ks)))
            for (l,k) in enumerate(ks)
                summands[l] = sin(k*pi*(1+x)/2) * 1/(k^3*sinh(k*pi)) * (sinh(k*pi*(1+y)/2) + sinh(k*pi*(1-y)/2))
            end
            u[i,j] += -16/(pi^3) * sum(summands)
        end
    end

    return u, coordinate_lattice
end

function main()
    N = 100
    h = 0.02

    uexact, coords_exact = exactPoisson(N)

    u_matrix_fd = finiteDifferencePoisson(N, h)

    plt.heatmap(uexact, title="Exact Solution")
    plt.savefig("exactPoisson.png")
    plt.heatmap(u_matrix_fd, title="Finite Difference Matrix")
    plt.savefig("FDM_Poisson.png")

end

main()

