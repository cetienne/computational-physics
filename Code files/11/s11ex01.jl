# Computational Physics
# Set 11, Exercise 01

# The Solar System and Integration

# Author(s): Etienne Corminboeuf

import LinearAlgebra as la
import Plots as plt

function leapFrog(f, phi, deltaT)

    phi_np1 = 2*deltaT .* f + phi

    return phi_np1
end

"""
    f and phi denote f(t_n), phi(t_n) where n denotes the previous timestep
"""
function timeStep(f, phi, deltaT, method="leapfrog")
    
    if method == "leapfrog"
        return leapFrog(f, phi, deltaT)

    else 
        error("time step method not implemented!")
    end

end

"""
    Attractive Newtonian gravitational force between Earth and Sun. 
    return: force acting on earth
"""
function f_es(re)
    G = 6.67408e-11
    me = 5.972e24
    ms = 1.989e30
    
    rabs = la.norm(re)

    f12 = -G * me * ms / rabs^3 .* re
    return f12
end

"""
    Returns the Newtonian gravitational attraction between planets i and j, acting on i.
"""
function f_ij(ri, rj, mi, mj)
    G = 6.67408e-11
    
    rabs = la.norm(ri .- rj)

    fij = -G * mi * mj / rabs^3 .* (ri .- rj)
    return fij
end

function integrate(timestepper, T, deltaT, f0, phi0, m, ms)

    N = Int64(ceil(T/deltaT))
    # phi = Array((size(phi0)..., N))
    # f = Array((size(phi0)..., N))
    no_planets = size(m, 1)

    phi = Array{Float64, 3}(undef, N+1, 2*no_planets,2)
    f = Array{Float64, 3}(undef, N+1, 2*no_planets,2)
    phi[1,:,:] = phi0
    f[1,:,:] = f0
    
    # we determine phi(t_1) using an explicit euler step
    phi1 = deltaT .* f0 + phi0
    f1 = Array{Float64, 2}(undef, 2*no_planets,2)
    for i in 1:2*no_planets
        if i % 2 != 0
            ind = Int64((i+1)/2)
            f1[i,:] = phi1[i+1,:] / m[ind]
        else
            ind = Int64(i/2)
            ri = phi[1,i-1,:]
            # add up contributions from planets and sun
            f1[i,:] = sum(f_ij(ri, phi[1,j,:], m[ind], m[j]) for j in deleteat!(collect(1:no_planets), ind)) + f_ij(ri, 0, m[ind], ms)
        end
    end

    
    # f1[1,:] = phi1[2,:] / me
    # f1[2,:] = f_es(phi1[1,:])

    # update the register
    for i in 1:2*no_planets
        phi[2,i,:] = phi1[i,:]
        f[2,i,:] = f1[i,:]
    end
    println("initialized memory")

    # phi[2,1,:] = phi1[1,:]
    # phi[2,2,:] = phi1[2,:]
    # f[2,1,:] = f1[1,:]
    # f[2,2,:] = f1[2,:]

    # start the time integration
    for n in 3:(N+1)
        if n % 1000 == 0
            println("calculating timestep $n (of $N)")
        end
        f_n = Array{Float64, 2}(undef, 2*no_planets,2)
        phi_n = timeStep(f[n-1,:,:], phi[n-1,:,:], deltaT)

        for i in 1:2*no_planets
            if i % 2 != 0
                ind = Int64((i+1)/2)
                f_n[i,:] = phi_n[i+1,:] / m[ind]
            else
                ind = Int64(i/2)
                ri = phi_n[i-1,:]
                # add up contributions from planets and sun
                f_n[i,:] = sum(f_ij(ri, phi_n[j,:], m[ind], m[j]) for j in deleteat!(collect(1:no_planets), ind)) + f_ij(ri, 0, m[ind], ms)
            end
        end

        # update the register
        for i in 1:2*no_planets
            phi[n,i,:] = phi_n[i,:]
            f[n,i,:] = f_n[i,:]
        end
        # phi[n,1,:] = phi_n[1,:]
        # phi[n,2,:] = phi_n[2,:]
        # f_n = Array{Float64, 2}(undef, 2,2)
        # f_n[1,:] = phi_n[2,:] / me
        # f_n[2,:] = f_es(phi_n[1,:])
        # f[n,1,:] = f_n[1,:]
        # f[n,2,:] = f_n[2,:]
    end

    return phi

end


function main()

    # global parameters
    R = 149597900000
    me = 5.972e24
    ms = 1.989e30
    mj = 1.898e27
    G = 6.67408e-11

    m = [me, mj]
    planet_labs = ["Earth", "Jupiter"]
    no_planets = size(m,1)

    ve_0 = 29.78e3
    vj_0 = 13.0697e3

    T = 11*365*24*60*60
    deltaT = 100

    # earth xy position
    # X = 3.740037000492308E+07 Y = 1.427624992894078E+08, VX=-2.924796967461710E+01 VY= 7.684670766752882E+00
    # jupiter xy position
    # X = 6.852339296636820E+08 Y =-2.954160681808675E+08, VX= 5.016715346928817E+00 VY= 1.261087383640502E+01

    r0 = transpose(hcat([[3.740037000492308e10, 1.427624992894078e11],
                 [6.852339296636820e11, -2.954160681808675e11]]...))
    v0 = transpose(hcat([[-2.924796967461710e4, 7.684670766752882e3],
                 [5.016715346928817e3, 1.261087383640502e4]]...))


    # phi0 = Array{Float64, 2}([[0, R], [-me * ve_0, 0]])
    phi0 = Array{Float64, 2}(undef, 2*no_planets,2)
    f0 = Array{Float64, 2}(undef, 2*no_planets,2)
    for i in 1:2*no_planets
        if i % 2 != 0
            ind = Int64((i+1)/2)
            phi0[i,:] = r0[ind,:]
            f0[i,:] = v0[ind,:]
        else
            ind = Int64(i/2)
            phi0[i,:] = m[Int64(i/2)] * v0[Int64(i/2),:]
            f0[i,:] = sum(f_ij(r0[ind,:], r0[j], m[ind], m[j]) for j in deleteat!(collect(1:no_planets),ind)) + f_ij(r0[ind,:], 0, m[ind], ms)
        end
    end

    # phi0[1,:] = r0
    # phi0[2,:] = me.*v0
    # phi0 = collect([r0, me .* v0])
    # f0 = Array{Float64, 2}(undef, 2*no_planets,2)
    # f0[1,:] = v0
    # f0[2,:] = f_es(r0)

    phi = integrate("leapfrog", T, deltaT, f0, phi0, m, ms)

    println("integration finished")

    # determine quantities from data

    # velocities
    (nx, ny, nz) = size(phi[:,2:2:2*no_planets,:])
    rs = zeros((nx, ny, nz))
    vs = zeros((nx, ny, nz))
    for i in 2:2:2*no_planets
        ind = Int64(i/2)
        vs[:,ind,:] = phi[:,i,:] ./ m[ind]
        rs[:,ind,:] = phi[:,i-1,:]
    end

    # v = phi[:,2,:] ./ me
    # r = phi[:,1,:]
    rabs = zeros((nx, ny))
    vsquared = zeros((nx, ny))

    Ekin = zeros((nx, ny))
    Epot = zeros((nx, ny))

    for j in 1:ny
        for i in 1:nx
            rabs[i, j] = la.norm(rs[i,j,:])
            vsquared[i, j] = la.norm(vs[i,j,:])^2
        end
        Ekin[:,j] = 0.5 * m[j] * vsquared[:,j]
        Epot[:,j] = 0.5 * G * me * ms./(rabs[:,j])
    end

    #Ekin = 0.5 * me * vsquared
    # Epot = 0.5 * G * me * ms./(rabs)

    tdomain = collect(0:deltaT:T) 

    println("creating output data")

    rpl = plt.plot()
    vpl = plt.plot()
    Ekinplot = plt.plot()
    Epotplot = plt.plot()
    Etotplot = plt.plot()

    for i in 1:ny
        plt.plot!(rpl, rs[:,i,:], lab=[planet_labs[i] * ",r_x" planet_labs[i] * ",r_y"])
        plt.plot!(vpl, vs[:,i,:], lab=[planet_labs[i] * ",v_x" planet_labs[i] * ",v_y"])
        plt.plot!(Ekinplot, tdomain, Ekin[:,i], xlabel="time [s]", ylabel="E [J]", title="Kinetic Energy", lab=planet_labs[i])
        plt.plot!(Ekinplot, tdomain, Epot[:,i], xlabel="time [s]", ylabel="E [J]", title="Potential Energy", lab=planet_labs[i])
        Etotrel = (Ekin[:,i] + Epot[:,i]) ./(Ekin[1,i] + Epot[1,i])
        plt.plot!(Etotplot, Etotrel, title="Total Energy variation", lab=planet_labs[i])
    end

    # rpl = plt.plot(r, title="r")
    
    # vpl = plt.plot(v, title="v")
    plt.savefig(rpl, "r_EJS.png")
    plt.savefig(vpl, "v_EJS.png")

    # Ekinplot = plt.plot(tdomain, Ekin, xlabel="time [s]", ylabel="E [J]", title="Kinetic Energy")
    # Epotplot = plt.plot(tdomain, Epot, xlabel="time [s]", ylabel="E [J]", title="Potential Energy")
    plt.savefig(Ekinplot, "Ekin_EJS.png")
    plt.savefig(Epotplot, "Epot_EJS.png")

    plt.savefig(Etotplot, "Etotvar_EJS.png")

    # Etrace = plt.scatter(tdomain, [Ekin, Epot])
    # Eplot = plt.plot(tdomain, Ekin .+ Epot , lab=["Ekin" "Epot"], xlabel="time [s]", ylabel="E [J]", title="Energy", marker="line")
    # plt.savefig(Eplot, "Eplot_ES.png")

end

main()