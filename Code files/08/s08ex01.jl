# Computational Physics
# Set 08, Exercise 01

# Monte Carlo Simulation of 2D Ising Model

# Author(s): Etienne Corminboeuf

import Random as rd
import Plots as plt
import Statistics as stats

global k = 1
global J = 1
global H = 10

function initializeGrid(L, d, initDistribution)
    dims = L * ones(d)
    dims = [UInt(d) for d in dims]
    dims = tuple(dims...)

    lattice = zeros(Int8, dims)

    if initDistribution == "groundstate"
        lattice = lattice .+ Int8(1)
    elseif initDistribution == "random"
        rd.rand!(lattice, [Int8(-1), Int8(1)])
    end

    return lattice
end

function Hamiltonian(lattice)
    # TODO: adapt for 3D

    Hamil = 0
    (Lx, Ly) = size(lattice)
    for i in 1:Lx
        for j in 1:Ly

            # spin interaction with magnetic field
            Hamil += -H * lattice[i,j]
            
            # spin-spin interaction with neighbour right
            if j == Ly
                Hamil += -J * lattice[i, 1] * lattice[i,j]
            else
                Hamil += -J * lattice[i, j+1] * lattice[i,j]
            end

            # spin-spin interaction with neighbour bottom
            if i == Lx
                Hamil += -J * lattice[1,j] * lattice[i,j]
            else
                Hamil += -J * lattice[i+1,j] * lattice[i,j]
            end

        end
    end
    
    return Hamil
end

function singleFlipMetropolis(lattice, E0, T)
    dims = size(lattice)
    
    # randomly sample a spin to flip
    flipInd = []
    for dim in dims
        push!(flipInd, rand(collect(1:dim)))
    end

    # flip it
    lattice[flipInd...] *= -Int8(1)

    # calculate the energy of the new system
    E = Hamiltonian(lattice)

    if E < E0
        # println("accepted, energy")
        accept = true
    else
        p = exp(-(E - E0)/(k*T))
        if rand() < p
            # println("accepted, chance")
            accept = true
        else
            # println("rejected, chance")
            accept = false
        end
    end

    if !accept
        # flip it back
        lattice[flipInd...] *= Int8(1)
        return lattice, E0
    end

    return lattice, E
end

function thermaliseSystem(lattice, nsteps, T)

    E0 = Hamiltonian(lattice)
    for i in 1:nsteps
        lattice, E0 = singleFlipMetropolis(lattice, E0, T)
    end

    # plt.heatmap(lattice)
end

function Magnetization(lattice)
    (Lx, Ly) = size(lattice)

    M = 0
    for i in 1:Lx
        for j in 1:Ly
            M += lattice[i,j]
        end
    end

    return M
end


function main()

    nsims = 100
    nthermsteps = 100
    L = 10
    d = 2
    initDistribution = "groundstate"

    lattice = initializeGrid(L, d, initDistribution)

    Ta = 0.2
    Tb = 4

    Es = []
    Ms = []
    Chis = []

    Temps = range(Ta, Tb; length=10)
    for T in Temps
        println("processing T=$T")
        EsT = []
        MsT = []

        for n in 1:nsims
            thermaliseSystem(lattice, nthermsteps, T)

            # calculate quantitiets

            E = Hamiltonian(lattice) # energy
            push!(EsT, E)

            M = Magnetization(lattice) # Magnetization
            push!(MsT, M)
        end

        plt.plot(1:nsims, [EsT, MsT], lab=["E" "M"])
        plt.savefig("EM_evol_T$T.png")

        ChiT = 1/(k*T) * (stats.mean(MsT.^2) - stats.mean(MsT)^2) # Susceptibility

        push!(Es, stats.mean(EsT))
        push!(Ms, stats.mean(MsT))
        push!(Chis, stats.mean(ChiT))

    end

    plt.plot(Temps, [Es, Ms, Chis.*1e-2], lab=["E" "M" "χ * 1e-2"], title="H=$H, nsims=$nsims, nthermsteps=$nthermsteps, L=$L")
    plt.savefig("EMCHI.png")
    # TODO: plots
    
end

main()